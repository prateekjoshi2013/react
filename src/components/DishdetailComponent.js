 import {Card,CardImg,CardBody,CardTitle,CardText} from 'reactstrap';

 const RenderComments = ({comments})=>{
   
    const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
    if(comments){
        return (
            <div className="p-3">
            <h3>Comments</h3>
            <ul className="list-unstyled">
                {comments.map(comment=>(<li key={comment.id}><p>{comment.comment}</p><p>{`-- ${comment.author} , ${new Date(comment.date).toLocaleDateString("en-US", options)}`} </p></li>))}
            </ul>
            </div>
        );
    }else{
        return <div></div>;
    }
}

const RenderDish = ({dish})=>{
    if (dish){
        return(
            <Card>
                <CardImg top src={dish.image} alt={dish.name} />
                <CardBody>
                  <CardTitle>{dish.name}</CardTitle>
                  <CardText>{dish.description}</CardText>
                </CardBody>
            </Card>
        );
    }else
        return <div></div>;
}

 const Dishdetail = (props)=>{
            if(props.dish){
                return ( 
                <div className='row'>
                    <div className="col-12 col-md-5 m-1">
                        {/* <RenderDish dish={dishDetails}/> */}
                        <RenderDish dish={props.dish}/>
                    </div>
                    <div className="col-12 col-md-5 m-1">
                        <RenderComments comments={props.dish.comments}/>
                    </div>
                </div>
                );
            }else{
                return <></>
            }
}

export default Dishdetail;